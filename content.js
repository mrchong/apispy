var stringifyEvent = (event) => {
  var ret = {};
  for (var i in event) {
    var data = event[i];
    if (typeof data !== 'object') {
      ret[i] = data;
    }
  }
  ret.querySelector = getCssSelectorShort(event.target);
  return ret;
}

const getCssSelectorShort = (el) => {
  let path = [], parent;
  while (parent = el.parentNode) {
    let tag = el.tagName.toLowerCase(), siblings;
    path.unshift(
      el.id ? `#${el.id}` : (
        siblings = parent.children,
        [].filter.call(siblings, sibling => sibling.tagName.toLowerCase() === tag).length === 1 ? tag :
        `${tag}:nth-child(${1+[].indexOf.call(siblings, el)})`
      )
    );
    el = parent;
  };
  return `${path.join(' > ')}`.toLowerCase();
};

var blackList = [
  "load",
  "pageshow",
  "deviceorientationabsolute",
  "devicemotion",
  "deviceorientation",
  "message",
  "pointerrawupdate",
  "pointerover",
  "mouseover",
  "pointermove",
  "mousemove",
  "pointerout",
  "mouseout",
  "wheel",
  "transitionend",
  "pointerdown",
  "selectstart",
  "pointerup",
  "blur",
  "beforeunload",
  "pagehide",
  "unload"
];

Object.keys(window).forEach(key => {
  if (/^on/.test(key)) {
    var subKey = key.substring(2,key.length);
    if (blackList.indexOf(subKey) >= 0) {
      return;
    }
    window.addEventListener(key.slice(2), event => {
      //if (blackList.indexOf(event.type) < 0) {
        var copiedEvent = stringifyEvent(event);
        /*
        var clipboardData = event.clipboardData || window.clipboardData;
        if (clipboardData) {
          console.log('pastedData: ', clipboardData, clipboardData.getData('Text'))
          copiedEvent.pastedData = clipboardData.getData('Text');
        }
        */
        /*
        if (copiedEvent.hasOwnProperty('AT_TARGET')) {
          return;
        }
        */
        chrome.runtime.sendMessage({
          from: 'content',
          subject: 'showPageAction',
          event: JSON.stringify(copiedEvent)
        });
      //} else {
      //  console.log('key: ', key);
      //}
    });
  }
});
/*
document.addEventListener('paste', e => {
  var clipboardData = e.clipboardData || window.clipboardData || navigator.clipboard;
  clipboardData.getText().then(text => {
    console.log('pasted: ', text);
    var copiedEvent = stringifyEvent(e);
    e.pastedData = text;
    chrome.runtime.sendMessage({
      from: 'content',
      subject: 'showPageAction',
      event: JSON.stringify(copiedEvent)
    });
    //ChromeSamples.log('Updated clipboard contents: ' + text);
  });
});
*/
