var attachedTabs = {};
const responses = {};
const blackList = {};
var actions = [];
let currentTab;
function chromeError() {
  if(chrome.runtime.lastError) {
    console.warn("Whoops.. " + chrome.runtime.lastError.message);
  }
}

function captureData(key, data) {
  chromeError();
  if (!responses[key]) {
    responses[key] = {};
  }
  responses[key] = {
    ...responses[key],
    ...data,
    lastUserEventAction1123: actions.length - 1
  };
}
function attachDebugger(details){
  chromeError();
  tabId = details.tabId;
  windowId = details.windowId;
  const hasWindow = attachedTabs[windowId];
  if (!hasWindow || hasWindow && attachedTabs[windowId][tabId]) {
    if (!hasWindow) {
      attachedTabs[windowId] = {};
    }
    attachedTabs[tabId] = true;
    chrome.debugger.attach({tabId:tabId}, "1.0", function() {
      chromeError();
      // console.log('errors3211: ', arguments);
    });
    chrome.debugger.sendCommand({ tabId:tabId }, "Network.enable");
    // chrome.debugger.sendCommand({ tabId:tabId }, "Network.getResponseBody");
    // chrome.debugger.sendCommand({ tabId:tabId }, "Network.getRequestPostData");

    function onEvent(debuggeeId, message, params) {
      chromeError();
      console.log('arguments: ', message, params);
      // console.log('arguments2246: ', tabId, arguments);
      if (message == "Network.requestWillBeSent" && params.type == "script") {
      // console.log("requestWillBeSent =" + JSON.stringify(params))
      }
      
      if (message === 'Network.requestWillBeSent') {
        //// console.log('requestWillBeSent3211b: ', params.url);
        // console.log('requestWillBeSent3211: ', params);
        const key = debuggeeId.tabId.toString() + params.requestId.toString();
        if(params.request.url.indexOf('https://ing-district.clicktale.net/') >= 0) {
          blackList[key] = true;
        } else {
          captureData(key, {
            request: params.request
          });
        }
      }
      if (message === 'Network.loadingFinished') {
        const key = tabId.toString() + params.requestId.toString();
        if(blackList[key]) {
          return;
        }
        chrome.debugger.sendCommand({
          tabId: tabId
        }, "Network.getResponseBody", {
          "requestId": params.requestId
        }, function(response) {
          chromeError();
          // console.log('arguments3211: ', arguments);
          captureData(key, {response});
          setTimeout(() => {
            // console.log('data3211: ', key, responses[key]);
          });
         // you get the response body here!
        });
        chrome.debugger.sendCommand({
          tabId: tabId
        }, "Network.getRequestPostData", {
          "requestId": params.requestId
        }, function(response) {
          chromeError();
          if (response) {
            captureData(tabId.toString() + params.requestId.toString(), {postRequestData: response.postData});
          }
          // you get the response body here!
        });
      }
      // Network.getAllCookies
      if (message == "Network.responseReceived"){
        const key = tabId.toString() + params.requestId.toString();
        if(blackList[key]) {
          return;
        }
        captureData(key, {responseMetaData: params.response});
    /*
        chrome.debugger.sendCommand({
           tabId: tabId
         }, "Network.getResponseBody", {
           "requestId": params.requestId
         }, function(response) {
           // console.log('response3211: ', response);
          // you get the response body here!
    
        });*/
      }
    }
    chrome.debugger.onEvent.addListener(onEvent);
  }
}

chrome.tabs.onActivated.addListener((data) => {
  // console.log('data: ', data);
  currentTab = data.tabId;
  attachDebugger(data);
});
/*
  chrome.webRequest.onBeforeRequest.addListener(attachDebugger,  {urls: ["<all_urls>"]});
  setTimeout(() => {
  // console.log('hello world');
  }, 4000);
  */

  //chrome.debugger.getTargets(function(){// console.log('arguments:', arguments)})

chrome.runtime.onMessage.addListener((msg, sender) => {
  // First, validate the message's structure.
  // console.log('msg: ', msg);
  if ((msg.from === 'content') && (msg.subject === 'showPageAction')) {
    // Enable the page-action for the requesting tab.
    actions.push(JSON.parse(msg.event));
    // console.log('event: ', msg.event);
    // chrome.pageAction.show(sender.tab.id);
  }
});
chrome.runtime.onMessage.addListener(
  function(request, sender, sendResponse) {
    // console.log('onMessage: ', request, sender, sendResponse);
    //if (request.greeting == "hello")
    //  sendResponse({farewell: "goodbye"});
  }
);