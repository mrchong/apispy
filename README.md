1. Download(and unzip)/clone apispy chrome extension from: https://bitbucket.org/mrchong/apispy/src/master/
2. Open chrome
3. Go to chrome://extensions
4. Enable "Developer mode" in the top right
5. Click "Load unpacked" in the top left
6. Choose the folder you just cloned/unzipped
7. There should now be a new extension called ApiSpy in your list of chrome extensions
8. Make sure it is enabled.
9. Click the "background page" link on the ApiSpy extension.
10. This will open a console that will let you view all the requests/responses.
11. Type "responses" without quotes into the console.

12. if you want to use it in incognito mode, then continue reading
13. Go back to the chrome://extensions tab
14. Click "Details" button on the ApiSpy extension
15. Scroll down and click "Allow in incognito"

NOTE: This will capture ANY network calls accross ANY tab. To reduce size of captured data, and avoid capturing PII/non-work related Passwords, only enable this extension when QAing.

NOTE: there will be a banner at the top of the page that says: "'ApiSpy' is debugging this browser". Do NOT cancel or close this banner, or you will have to turn the extension off/on again to re-enable it.